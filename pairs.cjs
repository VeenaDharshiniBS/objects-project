var _ = require('underscore'); 

function pairs(obj)
{
    if(obj === undefined || typeof obj !== 'object')
        return [];
    
    return _.pairs(obj);
}

module.exports = pairs;