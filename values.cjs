var _ = require('underscore'); 

function values(obj)
{
    if(obj === undefined || typeof obj !== 'object')
        return [];
    return _.values(obj);
}

module.exports = values;