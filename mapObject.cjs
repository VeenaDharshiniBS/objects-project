var _ = require('underscore'); 

function mapObject(obj,cb)
{
    if(typeof obj !== 'object' || typeof cb !== 'function' || obj === undefined || cb === undefined)
        return {};
    
    return _.mapObject(obj,cb);
}

module.exports = mapObject;