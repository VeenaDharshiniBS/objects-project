var _ = require('underscore'); 

function invert(obj)
{
    if(obj === undefined || typeof obj !== 'object')
        return [];
    return _.invert(obj);
}

module.exports = invert;