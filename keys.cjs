var _ = require('underscore'); 

function keys(obj)
{
    if(obj === undefined || typeof obj !== 'object')
        return [];
    return _.keys(obj);
}

module.exports = keys;