var _ = require('underscore'); 

function defaults(obj,defalutObj)
{
    if((defalutObj === undefined && obj === undefined) || (typeof obj != 'object' && typeof defalutObj !== 'object'))
        return [];
    if(typeof obj !== 'object')
        return defalutObj;
    return _.defaults(obj,defalutObj);
}

module.exports = defaults;