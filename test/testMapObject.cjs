const test = require('../mapObject.cjs');

const testObject = { name: 'Bruce Wayne', age: 36, location: 'Gotham' };

function cb(value) {
    return value;
}

console.log(test(testObject,cb));
console.log(test(testObject));
console.log(test(cb));
console.log(test("",cb));
console.log(test([]));
console.log(test());
